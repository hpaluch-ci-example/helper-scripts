# helper-scripts

CI Helper scripts that will be used across other CI Pipelines

You can look here for Package published on Package Registry:
- https://gitlab.com/hpaluch-ci-example/helper-scripts/-/packages

# Tips

To perform basic YAML validation install yamllint, for example using:

```bash
# installing yamllint on openSUSE 15.2 LEAP
sudo zypper in python3-yamllint
```

And run:

```bash
yamllint .gitlab-ci.yml
```

To run CI Pipeline locally (will fail on CURL step), try:

```bash
gitlab-runner exec shell package_tar_gz
```

