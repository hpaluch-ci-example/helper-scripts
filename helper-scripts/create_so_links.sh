#!/bin/bash

# create symlinks to all so libraries in specified directory

[ $# -gt 0 ] || {
	echo "Usage: $0 lib_directory ..." >&2
	echo "Example: $0 dist/lib"     >&2
	exit 1
}



set -e
for d in "$@"
do
	[ -n "$d" ] || {
		echo "Parameter may not be empty" >&2
		exit 1
	}
	[ -d "$d" ] || {
		echo "Specified path '$d' is NOT directory" >&2
		exit 1
	}
	cd "$d"
	for i in *.so.*[0-9]
	do
	 [ -r "$i" ] || {
		echo "Unable to find library: '$i'" >&2
		exit 1
	 }
	 blib="${i%.so.*}.so"
	 [ -L "$blib" ] || ln -sv $i "$blib"
	 SONAME=$(readelf -d $i | grep SONAME | sed 's/.*\[//;s/\].*//')
	 if [ -n "$SONAME" ] ; then
	   #echo "SONAME='$SONAME'"
	   # we also need SONAME link to main library if different
	   # NOTE: scratchbox ln does not support "-r" yet...
	   [ -L "$SONAME" ] || ln -sv $i $SONAME
	 fi
	done
done

exit 0

