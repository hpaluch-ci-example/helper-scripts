#!/bin/bash

# externalize_debug_symbols.sh - move debug symbols from
# binary to external *.debug file
#
# can be used for both ELF binary and *.so library

[ $# -eq 1 ] || {
    echo "Usage: $0 binary_or_library_with_debug_symbols" >&2
    exit 1
 }
 app="$1"
 [ -f "$app" ] || {
    echo "Pathname '$1' is not regular file" >&2
    exit 1
 }

 if ! file "$app" | fgrep -q ELF ; then
	 echo "File '$app' is not ELF executable/so file" >&2
	 exit 1
 fi

 [ -z "$CROSS_COMPILE" ] || ${CROSS_COMPILE}objcopy --help > /dev/null || {
 	echo "CROSS_COMPILE is set to '$CROSS_COMPILE' but '${CROSS_COMPILE}objcopy --help' returned error" >&2
	exit 1
}
 
 objcopy_compress=''
 if ${CROSS_COMPILE}objcopy --help | fgrep -q compress-debug-sections ; then
   objcopy_compress="--compress-debug-sections"
 fi
 set -ex
 ${CROSS_COMPILE}objcopy --only-keep-debug $objcopy_compress "$app" "$app.debug"
 case "$app" in
   *.so.*[0-9])
       ${CROSS_COMPILE}strip --strip-unneeded "$app"
      ;;
   *)
      ${CROSS_COMPILE}strip  "$app"
      ;;
 esac
 ${CROSS_COMPILE}objcopy --add-gnu-debuglink "$app.debug" "$app"
 ${CROSS_COMPILE}readelf -p .gnu_debuglink "$app" | strings
 set +xe

