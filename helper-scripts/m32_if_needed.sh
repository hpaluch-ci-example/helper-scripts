#!/bin/bash

# emits `-m32` if needed for current gcc compiler

CC=${CC:-gcc}
set -e

case "$($CC -dumpmachine)" in
	x86_64*)
		printf '%s' -m32
		;;	
	*)
		true
		;;
esac
exit 0

