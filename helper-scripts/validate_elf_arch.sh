#!/bin/bash

# validate that target ELF binary or *.a static library uses proper architecture

my_usage () {
	echo "Usage: $0 [ -m override_machine_name ] elf_binary1 ..." >&2
	exit 1
}

[ $# -gt 0 ] || {
	my_usage
}

machine=''
while [ $# -gt 0 ]
do
	case "$1" in
		-m)
			shift
			machine="$1"
			[ $# -gt 0 ] || {
				echo "Parameter '-m' requires value">&2
				my_usage
			}
			shift
			;;
		-*)
			echo "ERROR: Unknown option '$1'" >&2
			exit 1
			;;
		*)
			break
			;;
	esac
done

[ $# -gt 0 ] || {
	echo "ERROR: No ELF file argument specified" >&2
	my_usage
}

if [ -n "$machine" ]; then
	:
elif [ -n "$CROSS_COMPILE" ]; then
	# remove path
	cross_name="${CROSS_COMPILE##*/}"
	# keep only word before 1st dash
	machine="${cross_name%%-*}"
	[ -n "$machine" ] || {
		echo "Unable to extract Machine from CROSS_COMPILE='$CROSS_COMPILE'">&2
		exit 1
	}
else
	machine=$(uname -m)
fi


case "$machine" in
	x86_64)
		ELF_MACHINE=X86-64
		;;
	i[3456]86)
		ELF_MACHINE=80386
		;;
	arm)
		ELF_MACHINE=ARM
		;;
	# aarch64 is often extracted from CROSS_COMPILE variable
	aarch64)
		ELF_MACHINE=AArch64
		;;
	*)
		echo "ERROR: Unable to map architecture from MACHINE '$machine'" >&2
		exit 1
		;;
esac


while [ $# -gt 0 ]
do
	i="$1"
	shift
	[ -r "$i" ] || {
		echo "ERROR: unable to read '$i'" >&2
		exit 1
	}
	# WARNING! readelf may report more than one line for *.a !!!
	#          Therefore we use negative match...
	${CROSS_COMPILE}readelf -h "$i" | fgrep Machine: | awk '{ print $(NF)}' | grep -v $ELF_MACHINE
	case "${PIPESTATUS[@]}" in
		"0 0 0 1")
			echo "OK: File '$i' has Machine: $ELF_MACHINE"
			: # OK
			;;
		"0 0 0 0")
			echo "Above Machine mismatch for '$i', expected: '$ELF_MACHINE'" >&2
			exit 1
			;;
		*)
			echo "Some Pipe commands failed, statues are: ${PIPESTATUS[@]}" >&2
			exit 1
			;;

	esac
done
exit 0


